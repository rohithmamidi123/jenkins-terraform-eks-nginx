#!/bin/bash
# this is for amazon linux 2 instance
# install gitlab
# Update the system
sudo dnf install -y policycoreutils-python-utils openssh-server openssh-clients perl
# Check if OpenSSH server daemon is enabled
sudo systemctl status sshd
## If OpenSSH server daemon is not enabled, enable it
sudo systemctl enable sshd
sudo systemctl start sshd
# Check if opening the firewall is needed
sudo systemctl status firewalld
## If firewalld is running, open the firewall for http and https connections
sudo firewall-cmd --permanent --add-service=http
sudo firewall-cmd --permanent --add-service=https
sudo systemctl reload firewalld

#installing postfix
sudo dnf install postfix
sudo systemctl enable postfix
sudo systemctl start postfix

#gitlab package repo

curl https://packages.gitlab.com/install/repositories/gitlab/gitlab-ee/script.rpm.sh | sudo bash

# Install GitLab
sudo dnf install -y gitlab-ee

# then install git
sudo yum install git -y

#then install terraform
sudo yum install -y yum-utils
sudo yum-config-manager --add-repo https://rpm.releases.hashicorp.com/AmazonLinux/hashicorp.repo
sudo yum -y install terraform

#finally install kubectl
sudo curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.23.6/bin/linux/amd64/kubectl
sudo chmod +x ./kubectl
sudo mkdir -p $HOME/bin && sudo cp ./kubectl $HOME/bin/kubectl && export PATH=$PATH:$HOME/bin
