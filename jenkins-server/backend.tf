terraform {
  backend "s3" {
    bucket         = "rohith-cicd-tf-eks"
    key            = "gitlab/terraform.tfstate"
    region         = "us-east-1"
    encrypt        = true
    dynamodb_table = "terraform-lock"
  }
}